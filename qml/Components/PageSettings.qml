/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

import QtQuick.LocalStorage 2.0


Page {
    id: settingsPage
    objectName: "settingsPage"
    anchors.fill: parent

    header: HeaderSettings {
        id: pageHeader
        title: i18n.tr("Settings")
        flickable: settingsFlickable
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: settingsFlickable
    }

    Flickable {
        id: settingsFlickable
        width: parent.width
        height: parent.height
        contentHeight: mainColumn.height
        topMargin: units.gu(2)
        bottomMargin: units.gu(4)

        Column {
            id: mainColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)
            spacing: units.gu(2)

            Text {
                text: i18n.tr("<b>Theme</b>")
                color: mainColor //theme.palette.normal.overlayText
                width:  parent.width - units.gu(2)
                anchors.horizontalCenter: parent.horizontalCenter
            }

            ListItem {
                id: themeTF
                width: parent.width
                height: cTheme.height > 0 ? cTheme.height : units.gu(7)
                divider.visible: false

                property string themeName: "TOTP"

                ChooserTheme {
                    id: cTheme
                    width: parent.width

                    onSelectedThemeChanged: themeTF.themeName = selectedTheme
                }
            }
        }
    }

    Component.onCompleted: {
        //Load settings from config
    }
}
