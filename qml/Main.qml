import QtQuick 2.9
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3
import QtQuick.LocalStorage 2.0

//Only need to import OTPAuth once
import "js/otpauth.min.js" as OTPAuth
import "js/db.js" as KeysDB
import "Components"

MainView {
    id: root
    objectName: 'mainView'
    anchorToKeyboard: true

    /* If applicationName starts by a number the UrlDispatcher
     * ignores the incoming link
     * See: https://github.com/ubports/url-dispatcher/issues/7
     */
    applicationName: 'tfamanager.cibersheep'
    automaticOrientation: true

    property var pepe
    property string uriToRender
    property bool resumeIsMainPage: true
    property bool isResumeFlicking: false

    //App Color Properties
    readonly property string      mainColor: "#594092"
    readonly property string secondaryColor: "#7b66b5"
    readonly property string highlightColor: "#447b66b5"

    signal initDB()

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: theming
        category: "Theming Settings"
        property string themeName: ""

        onThemeNameChanged: {
            setTheme();
            bottomEdge.refresh();
        }
    }

    PageStack {
        id: mainStack
        anchors.fill: parent

        onCurrentPageChanged: {
            console.log("Page: " + currentPage.objectName)
            resumeIsMainPage = currentPage.objectName == "resumePage"
        }
    }

    BottomEdge {
        id: bottomEdge
        enabled: resumeIsMainPage && !isResumeFlicking
        height: root.height
        hint.visible: enabled
        hint.text: i18n.tr("Add Key")

        contentUrl: Qt.resolvedUrl("Components/PageEditAddKey.qml")

        //Workaround to refresh BottomEdge when theme changes
        function refresh() {
            commit();
            collapse();
        }
    }

    //Handle incoming otpauth:// url
    Connections {
        target: UriHandler

        onOpened: {
            if (uris.length > 0) {
                for (var i = 0; i < uris.length; i++) {
                    if (uris[i].match(/^otpauth/)) {
                        console.log('URI from UriHandler');
                        renderOTPuri(uris[i]);
                    }
                }
            }
        }
    }

    Arguments {
        id: args

        Argument {
            name: 'url'
            help: i18n.tr('Received oauth URL')
            required: false
            valueNames: ['URL']
        }
    }

    Component.onCompleted: {
        //Check if opened the app because we have an incoming uri
        if (args.values.url && args.values.url.match(/^otpauth/)) {
            console.log("Received otpauth URI as args.values.url")
            renderOTPuri(args.values.url);

        } else if (Qt.application.arguments && Qt.application.arguments.length > 0) {
            for (var i = 0; i < Qt.application.arguments.length; i++) {

                if (Qt.application.arguments[i].match(/^otpauth/)) {
                    console.log("Received otpauth URIs as Qt.application.arguments")
                    renderOTPuri(Qt.application.arguments[i]);
                }
            }
        }

        setTheme();
        mainStack.push(storedKeysComponent);
    }

    Component {
        //Pop up to add a key
        id: renderUriComponent

        RenderUriPopUp {}
    }

    Component {
        //Delegate of ListItem of codes
        id: keysDelegate

        KeysDelegate {}
    }

    Component {
        //ListItem of codes
        id: storedKeysComponent

        PageKeysResume {
            id: storedKeysPage
            objectName: "resumePage"

            //onIsAtTopChanged: isResumeFlicking = !isAtTop
        }
    }

    Timer {
        //General ticker. It ticks every second
        id: timer
        running: true
        repeat: true

        property int time

        onTriggered: time = Math.round(new Date().getTime() / 1000.0)
    }

    Timer {
        //Wait 250 miliseconds to load the BottomEdge content. Should improve app start time
        id: botEdgeTimer
        interval: 250
        running: true
        repeat: false

        onTriggered: bottomEdge.preloadContent = true
    }

    function renderOTPuri(uri) {
        /* uriToRender is a general var, otherwise if apps opens from UrlDispatcher,
         * PopUtils.open(renderUriComponent, caller, var) complains about caller not being ready
         * See issue: https://github.com/ubports/ubuntu-ui-toolkit/issues/43
         */
        uriToRender = uri
        PopupUtils.open(renderUriComponent)
    }

    function setTheme() {
        theme.name = theming.themeName
    }
}
