/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

Page {
    id: editAddKeyPage

    property var uriJson
    /*
    property string authType: keyPeriod.visible
        ? "TOTP"
        : "HTOP"*/
    //title: i18n.tr("Add Authenticator Key")
    //text: i18n.tr("%1 authenticator key").arg(authType)
    header: HeaderBase {
        id: pageHeader
        title: i18n.tr("Key Information")
        //flickable: editAddFlickable
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: editAddFlickable
    }

    Flickable {
        id: editAddFlickable
        width: parent.width
        height: parent.height
        contentHeight: mainColumn.height
        topMargin: Qt.inputMethod.visible ? Qt.inputMethod.keyboardRectangle.height + units.gu(2) : pageHeader.height + units.gu(2)
        bottomMargin: units.gu(4)

        Column {
            id: mainColumn
            width: parent.width
            spacing: units.gu(2)

            KeyToEdit {
                id: info
                width: parent.width * .85
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Button {
                width: Math.min(parent.width * .7, units.gu(39))
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Add")
                color: theme.palette.normal.positive
                enabled: !info.isInfoEmpty
                onClicked: addKey()
            }

            Button {
                width: Math.min(parent.width * .7, units.gu(39))
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Cancel")
                onClicked: closingPop()
            }
        }
    }

    function addKey() {
        //KeysDB.storeKey( Date(), uriToRender, userDdescription.text);
        var editedKey = info.checkFields();

        if (info.isTOTP) {
            editedKey = new OTPAuth.TOTP(editedKey);
        } else {
            editedKey = new OTPAuth.HOTP(editedKey);
        }

        KeysDB.storeKey( Date(), editedKey.toString(), info.userDesc);
        root.initDB();
        closingPop();
    }

    function closingPop() {
        bottomEdge.collapse();
    }

    Component.onCompleted: {
        //uriJson = OTPAuth.URI.parse(root.uriToRender);
    }

    Component.onDestruction: {
        //To be used when PopupBase is closed
    }

}
