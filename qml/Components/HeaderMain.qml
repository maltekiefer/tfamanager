/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

HeaderBase {
    trailingActionBar {
        actions: [
            Action {
                iconName: "settings"
                text: i18n.tr("Settings")

                onTriggered: {
                    mainStack.push(Qt.resolvedUrl("PageSettings.qml"));
                }
            }/*,
            Action {
                iconName: "search"
                text: i18n.tr("Search")

                onTriggered: {
                }
            }*/
       ]
    }
}
