/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Column {
    spacing: units.gu(2)

    property var uriJson
    property bool isTOTP: typeTF.keyType == "TOTP"
    property bool isInfoEmpty: isTOTP
        ? userDescTF.text == "" || issuerTF.text == "" || userTF.text == "" || intTF.text == "" || secretTF.text == ""
        : userDescTF.text == "" || issuerTF.text == "" || userTF.text == "" || countTF.text == "" || secretTF.text == ""
    property alias userDesc: userDescTF.text

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.top:parent.top
            anchors.topMargin: units.gu(2)
            id: typeTxt
            text: i18n.tr("<b>Type</b>")
            color: theme.palette.normal.overlayText
        }

        ListItem {
            id: typeTF
            width: parent.width - units.gu(2) - typeTxt.width
            height: cType.height > 0 ? cType.height : units.gu(7)
            divider.visible: false

            property string keyType: "TOTP"

            ChooserType {
                id: cType
                width: parent.width

                onSelectedTypeChanged: typeTF.keyType = selectedType
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: descTxt
            text: i18n.tr("<b>Description</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - descTxt.width
            id: userDescTF
            placeholderText: i18n.tr("Add a description")

            onAccepted: {
                focus = false;
                issuerTF.focus = true;
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: issuerTxt
            text: i18n.tr("<b>Issuer</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - issuerTxt.width
            id: issuerTF
            placeholderText: i18n.tr("Who is providing the service")

            onAccepted: {
                focus = false;
                userTF.focus = true;
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: usrTxt
            text: i18n.tr("<b>User</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - usrTxt.width
            id: userTF
            placeholderText: i18n.tr("Who is using the service")

            onAccepted: {
                focus = false;
                passLenTF.focus = true;
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.top:parent.top
            anchors.topMargin: units.gu(2)
            id: algTxt
            text: i18n.tr("<b>Algorithm</b>")
            color: theme.palette.normal.overlayText
        }

        ListItem {
            id: algTF
            width: parent.width - units.gu(2) - algTxt.width
            height: cAlg.height > 0 ? cAlg.height : units.gu(7)
            divider.visible: false

            property string algorithm: "SHA1"

            ChooserAlgorithm {
                id: cAlg
                width: parent.width

                onSelectedAlgChanged: algTF.algorithm = selectedAlg
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: passLenTxt
            text: i18n.tr("<b>Password Length</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - passLenTxt.width
            id: passLenTF
            placeholderText: i18n.tr("6-9 digit password")
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator{bottom: 6; top: 10;}
            onTextChanged: if (!acceptableInput) text = "";
            onAccepted: {
                focus = false;
                isTOTP
                    ? intTF.focus = true
                    : countTF.focus = true
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)
        visible: isTOTP

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: keyPeriod
            visible: true //uriJson.counter == undefined
            text: i18n.tr("<b>Interval</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - keyPeriod.width
            id: intTF
            placeholderText: i18n.tr("Password life in seconds")
            inputMethodHints: Qt.ImhDigitsOnly

            onAccepted: {
                focus = false;
                secretTF.focus = true;
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)
        visible: !isTOTP

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: countTxt
            //visible: !keyPeriod.visible
            text: i18n.tr("<b>Counter</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - countTxt.width
            id: countTF
            placeholderText: i18n.tr("Password initial generation")
            inputMethodHints: Qt.ImhDigitsOnly

            onAccepted: {
                focus = false;
                secretTF.focus = true;
            }
        }
    }

    Text {
        text: i18n.tr("<b>Secret Key</b>")
        color: theme.palette.normal.overlayText
    }

    TextArea {
        id: secretTF
        width: parent.width
        placeholderText: i18n.tr("Secret key base")
        autoSize: true
        wrapMode: Text.WrapAnywhere
    }

    function checkFields() {
        var isDescEmpty = userDescTF.text == "";
        var isIssuerEmpty = issuerTF.text == "";
        var isUserEmpty = userTF.text == ""
        var isIntEmpty = intTF.text == "";
        var isCountTFEmpty = countTF.text == "";
        var isSecretTFEmpty = secretTF.text == "";

        //algTF.algorithm

        console.log(isDescEmpty, isIssuerEmpty, isIntEmpty, isCountTFEmpty, isSecretTFEmpty, algTF.algorithm)
        if (isTOTP) {
            var returnKey = {
                issuer: issuerTF.text,
                label: userTF.text,
                algorithm: algTF.algorithm,
                digits: passLenTF.text,
                period: intTF.text,
                secret: secretTF.text // or "OTPAuth.Secret.fromB32('NB2W45DFOIZA')
            }
        } else {
            var returnKey = {
                issuer: issuerTF.text,
                label: userTF.text,
                algorithm: algTF.algorithm,
                digits: passLenTF.text,
                counter: countTF.text,
                secret: secretTF.text // or "OTPAuth.Secret.fromB32('NB2W45DFOIZA')
            }
        }

        return returnKey;
    }
}
