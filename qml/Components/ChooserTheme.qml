/*
 * Copyright (C) 2019 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0

Item {
    height: themeSetting.height

    property string selectedTheme: theming.themeName == ""
        ? "System Theme"
        : theming.themeName == "Ubuntu.Components.Themes.Ambiance"
            ? "Ambiance"
            : "Suru Dark"

    ListModel {
        id: themeModel
        Component.onCompleted: initialize()

        function initialize() {
            themeModel.append({"text": "Ambiance", "theme": "Ubuntu.Components.Themes.Ambiance"});
            themeModel.append({"text": "Suru Dark", "theme": "Ubuntu.Components.Themes.SuruDark"});
            themeModel.append({"text": "System Theme", "theme": ""});
        }
    }

    ExpandableListItem {
        id: themeSetting
        listViewHeight: themeModel.count*units.gu(6.1)
        model: themeModel
        title.text: selectedTheme

        delegate: StandardListItem {
            title.text: model.text
            icon.name: "ok"
            icon.visible: selectedTheme === model.text
            onClicked: {
                selectedTheme = model.text;
                theming.themeName = model.theme;
                themeSetting.toggleExpansion();
            }
        }
    }
}
