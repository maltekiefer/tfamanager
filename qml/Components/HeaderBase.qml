/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

PageHeader {
    StyleHints {
        foregroundColor: UbuntuColors.porcelain
        backgroundColor: mainColor
        //dividerColor: UbuntuColors.slate
    }
}
